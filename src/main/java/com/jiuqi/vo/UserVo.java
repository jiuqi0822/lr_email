package com.jiuqi.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/04
 * @Emm...
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserVo {
    private String username;

    private String password;

    private String email;
    //    验证码
    private String code;

    //省略了get和set方法，自己生成一下
}

