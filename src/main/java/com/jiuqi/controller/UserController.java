package com.jiuqi.controller;

import com.jiuqi.pojo.User;
import com.jiuqi.service.MailService;
import com.jiuqi.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/04
 * @Emm...
 */

@Controller
public class UserController {

    @Autowired
    private MailService mailService;

    @RequestMapping({"/", "/index"})
    public String index() {
        return "login";
    }

    @RequestMapping("/toReg")
    public String toReg() {
        return "register";
    }

    @RequestMapping("/toPass")
    public String toPass() {
        return "password";
    }

    /**
     * 发送注册验证码
     * @param email
     * @param httpSession
     * @return
     */
    @PostMapping("/sendEmail/{email}")
    @ResponseBody
    public String sendEmail(@PathVariable String email, HttpSession httpSession) {
        boolean flag = mailService.sendMimeMail(email, httpSession);
        return flag ? "success" : "false";
    }

    /**
     * 注册
     * @param userVo
     * @param session
     * @return
     */
    @PostMapping("/register")
    @ResponseBody
    public String register(UserVo userVo, HttpSession session) {
        boolean flag = mailService.registered(userVo, session);
        session.removeAttribute("code");
        return flag ? "success" : "false";
    }

    //@PostMapping("/login")
    //@ResponseBody
    //public String login(String email, String password) {
    //    boolean flag = mailService.loginIn(email, password);
    //    System.out.println(flag);
    //    return flag ? "success" : "false";
    //}

    @PostMapping("/login")
    public String login(User user) {
        boolean flag = mailService.login(user);
        return flag ? "success" : "false";
    }


    @PostMapping("/verify")
    @ResponseBody
    public String verify(User user) {

        return mailService.selectUserByUsername(user);
    }


    /**
     * 发送找回密码验证码
     * @param email
     * @param httpSession
     * @return
     */
    @PostMapping("/sendEmail2/{email}")
    @ResponseBody
    public String sendEmail2(@PathVariable String email, HttpSession httpSession) {
        boolean flag = mailService.sendMimeMail2(email, httpSession);
        return flag ? "success" : "false";
    }

    /**
     * 修改密码
     * @param userVo
     * @param session
     * @return
     */
    @PostMapping("/update")
    @ResponseBody
    public String update(UserVo userVo, HttpSession session) {
        boolean flag = mailService.updateUser(userVo, session);
        session.removeAttribute("code");
        return flag ? "success" : "false";
    }
}





























