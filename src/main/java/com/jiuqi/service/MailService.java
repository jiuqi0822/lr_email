package com.jiuqi.service;

import com.jiuqi.pojo.User;
import com.jiuqi.vo.UserVo;

import javax.servlet.http.HttpSession;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/04
 * @Emm...
 */

public interface MailService {

    /**
     * 给前端输入的邮箱，发送验证码
     *
     * @param email
     * @param session
     * @return
     */
    boolean sendMimeMail(String email, HttpSession session);

    /**
     * 随机生成6位数的验证码
     *
     * @return String code
     */
    String randomCode();

    /**
     * 检验验证码是否一致
     *
     * @param userVo
     * @param session
     * @return
     */
    boolean registered(UserVo userVo, HttpSession session);

    /**
     * 通过输入email查询password，然后比较两个password，如果一样，登录成功
     *
     * @param email
     * @param password
     * @return
     */
    boolean loginIn(String email, String password);

    /**
     * 登录
     * 检验用户名和密码
     *
     * @param user
     * @return
     */
    boolean login(User user);

    /**
     * 校验用户名和邮箱
     * @param user
     * @return
     */
    String selectUserByUsername(User user);

    /**
     * 通过邮箱找回密码
     * @param email
     * @param session
     * @return
     */
    boolean sendMimeMail2(String email, HttpSession session);

    /**
     * 校验验证码并重置更改密码
     * @param userVo
     * @param session
     * @return
     */
    boolean updateUser(UserVo userVo, HttpSession session);
}


























