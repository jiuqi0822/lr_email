package com.jiuqi.service.impl;

import com.jiuqi.mapper.UserMapper;
import com.jiuqi.pojo.User;
import com.jiuqi.service.MailService;
import com.jiuqi.vo.UserVo;
import com.jiuqi.vo.UserVoToUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Random;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/05
 * @Emm...
 */
@Service
public class MailServiceImpl implements MailService {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private UserMapper userMapper;

    //application.properties中已配置的值
    @Value("${spring.mail.username}")
    private String from;

    /**
     * 发送邮箱
     *
     * @param email
     * @param session
     * @return
     */
    @Override
    public boolean sendMimeMail(String email, HttpSession session) {
        try {
            //邮箱信息
            SimpleMailMessage mailMessage = new SimpleMailMessage();

            //主题
            mailMessage.setSubject("lr_email注册");

            //生成随机数
            String code = randomCode();

            System.out.println("code = " + code);

            //将随机数放置到session中
            session.setAttribute("email", email);
            session.setAttribute("code", code);

            //内容
            mailMessage.setText("lr_email注册的验证码是：" + code);

            //发给谁
            mailMessage.setTo(email);

            //你自己的邮箱
            mailMessage.setFrom(from);

            //发送
            mailSender.send(mailMessage);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 6位数验证码
     *
     * @return
     */
    @Override
    public String randomCode() {
        StringBuilder str = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            str.append(random.nextInt(10));
        }
        return str.toString();
    }

    /**
     * 检验验证码是否一致
     *
     * @param userVo
     * @param session
     * @return
     */
    @Override
    public boolean registered(UserVo userVo, HttpSession session) {
        //获取session中的验证信息
        String email = (String) session.getAttribute("email");
        String code = (String) session.getAttribute("code");

        //获取表单中的提交的验证信息
        String voCode = userVo.getCode();

        //如果email数据为空，或者不一致，注册失败
        if (email == null || email.isEmpty()) {
            return false;
        } else if (!voCode.equals(code)) {
            return false;
        }

        //保存数据
        User user = UserVoToUser.toUser(userVo);

        //将数据写入数据库
        userMapper.insertUser(user);

        //跳转成功页面
        return true;
    }

    @Override
    public boolean loginIn(String email, String password) {
        User user = userMapper.queryByEmail(email);

        if (user != null) {
            if (!user.getPassword().equals(password)) {
                return false;
            }
            System.out.println("登录成功:数据库密码是：" + user.getPassword());

            return true;
        }
        return false;
    }

    /**
     * 登录
     *
     * @param user
     * @return
     */
    @Override
    public boolean login(User user) {
        return userMapper.selectUser(user) != null ? true : false;
    }

    /**
     * 判断用户和邮箱是否存在
     *
     * @param user
     * @return
     */
    @Override
    public String selectUserByUsername(User user) {
        User user1 = userMapper.selectUserByUsername(user);
        System.out.println(user1);
        return user1 != null ? "SUCCESS" : "ERROR";
    }

    @Override
    public boolean sendMimeMail2(String email, HttpSession session) {
        try {
            //邮箱信息
            SimpleMailMessage mailMessage = new SimpleMailMessage();

            //主题
            mailMessage.setSubject("lr_email找回密码");

            //生成随机数
            String code = randomCode();

            System.out.println("code = " + code);

            //将随机数放置到session中
            session.setAttribute("email", email);
            session.setAttribute("code", code);

            //内容
            mailMessage.setText("lr_email找回密码的验证码是：" + code);

            //发给谁
            mailMessage.setTo(email);

            //你自己的邮箱
            mailMessage.setFrom(from);

            //发送
            mailSender.send(mailMessage);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean updateUser(UserVo userVo, HttpSession session) {
        //获取session中的验证信息
        String email = (String) session.getAttribute("email");
        String code = (String) session.getAttribute("code");

        //获取表单中的提交的验证信息
        String voCode = userVo.getCode();

        //如果email数据为空，或者不一致，注册失败
        if (email == null || email.isEmpty()) {
            return false;
        } else if (!voCode.equals(code)) {
            return false;
        }

        //保存数据
        User user = UserVoToUser.toUser(userVo);

        //将数据写入数据库
        userMapper.updateUser(user);

        //跳转成功页面
        return true;
    }


}

























