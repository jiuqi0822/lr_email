package com.jiuqi.mapper;

import com.jiuqi.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * @Author JiuQi 玖柒
 * @Date 2021/11/04
 * @Emm...
 */

@Mapper
@Repository
public interface UserMapper {

    /**
     * 注册，插入数据
     * @param user
     */
    void insertUser(User user);

    /**
     * 根据邮箱查询
     * @param email
     * @return
     */
    User queryByEmail(String email);

    /**
     * 登录
     * @param user
     * @return
     */
    User selectUser(User user);

    /**
     * 检验username和email是否存在
     * @param user
     * @return
     */
    User selectUserByUsername(User user);

    void updateUser(User user);
}

